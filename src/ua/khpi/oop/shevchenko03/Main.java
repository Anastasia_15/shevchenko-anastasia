package ua.khpi.oop.shevchenko03;

/**
 * ���� Main �������� ����� ���������, ������: ����� main
 * 
 * @author Shevchenco Anastasia
 * @version 1.0 05/10/19
 */
public class Main {

	/**
	 * M���� main ������� �������
	 * <p>
	 * ������������ ��������� �� 1. ���������� ����� str � ��� �������� ��������
	 * ���� 2. ������ ������ DeleteCharacter.
	 * 
	 * @return ��� void ������ �� �������. ������ ���������� ��������.
	 */
	public static void main(String[] args) {

		String str = "Jl,f@@@g!jjfj jf!!kgj?  grr,kgj    grgd    f rh?h r@@et   ghh@@@@dg.";
		System.out.println(str);

		HelperClass.DeleteCharacter(str);

	}

}
