package ua.khpi.oop.shevchenko03;

/**
 * ���� HelperClass ����� ���� ������ � ���: 1. ����� DeleteCharacter 2. �����
 * removeWhiteSpaces
 * 
 * @author Shevchenco Anastasia
 * @version 1.0 05/10/19
 */
public class HelperClass {

	/**
	 * M���� DeleteCharacter
	 * <p>
	 * ������������ ��������� �� 1. ���������� ��������� ������(������� ��
	 * ����� ��������) 2. � ���� ���������� ��������� �������� ������� 3.
	 * ��������� ����� ��� ������ ������� 4. ������ ������ removeWhiteSpaces
	 * 
	 * @return ������� ���� ���������� � ������ �����;
	 */
	public static String DeleteCharacter(String s) {
	    String charS = "";
	    char c1 = '!';
	    char c2 = '?';
	    char c3 = '@';

	    for (int i = 0; i < s.length(); i++) {
	      if (s.charAt(i) != c1 && s.charAt(i) != c2 && s.charAt(i) != c3) {
	        charS += s.charAt(i);
	      }
	    }
	    return charS;
	  }

	/**
	 * M���� removeWhiteSpaces
	 * <p>
	 * ������������ ��������� �� 1. ���������� ��������� ������ (end,spaceIndex)
	 * 2. ��������� ������ ��������. ���� ������� �� ������� �� ���� ��������
	 * ��������� ���� 1. 3. ��������� ����� ��� ������ ��������
	 * 
	 * @return ��� void ������ �� �������. ������ ���������� ������.
	 */
	static void removeWhiteSpaces(StringBuffer sb) {
		int end = 0;
		int spaceIndex = -1;
		char c1 = ',';
		char c2 = ' ';
		String c3 = ",";
		StringBuilder result = new StringBuilder();
		boolean prevCharIsDelimiter = false;
		for (int i = 0; i < sb.length(); i++) {
			if (!Character.isWhitespace(sb.charAt(i))) {
				sb.setCharAt(end++, sb.charAt(i));
				spaceIndex = -1;
			} else if (spaceIndex < 0) {
				sb.setCharAt(end++, sb.charAt(i));
				spaceIndex = i;
			}

		}

		for (int i = 0; i < sb.length(); i++) {
			char ch = sb.charAt(i);

			if (c3.indexOf(ch) != -1)
				prevCharIsDelimiter = true;
			else {
				if (ch != ' ' && prevCharIsDelimiter) {
					result.append(' ');
				}

				prevCharIsDelimiter = false;
			}
			result.append(ch);
		}

		System.out.println(result);
	}
}
