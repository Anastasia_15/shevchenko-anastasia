package ua.khpi.oop.shevchenko05;

import java.util.Arrays;

public class ConteinerClass implements Iterable<String> {
	final int size = 3;
	String mas[] = new String[size];
	String current = mas[0];
	int currentSize = 0;

	@Override
	public IteratorClass iterator() {
		// TODO Auto-generated method stub
		return new IteratorClass(this);
	}

	@Override
	public String toString() {// ������� ���� ���������� � ������ �����;
		return String.format("The array");
	}

	void add(String string) {
		currentSize = size();
		if (currentSize < size) {
			mas[currentSize] = string;
		}

		else {
			mas = Arrays.copyOf(mas, size * 2);
			mas[currentSize] = string;
		}
	}

	void clear() {
		mas = new String[size];
	}

	boolean remove(String string) {
		currentSize = size();
		int r = 0;
		for (int i = 0; i < currentSize; i++) {
			if (mas[i] != string) {
				r = r + 1;
			} else
				break;
		}
		for (int i = r; i < currentSize; i++) {
			if (mas[i] == string) {
				mas[i] = mas[i + 1];
				--currentSize;
			}
		}
		return false;
	}

	Object[] toArray() {
		Object[] object = mas;
		return object;
	}

	int size() {
		currentSize = 0;
		for (int i = 0; i < mas.length; i++) {
			if (mas[i] != null)
				currentSize++;
		}
		return currentSize;
	}

	boolean contains(String string) {
		for (int i = 0; i < size; i++) {
			if (mas[i] == string)
				return true;
		}
		return false;
	}

	boolean containsAll(ConteinerClass container, ConteinerClass container1) {
		int t = 0;
		for (int i = 0; i < size; i++) {
			if (container.mas[i].equals(container1.mas[i])) {
				t = t + 1;
			}
		}
		if (t == size) {
			return true;
		} else
			return false;
	}

	public static String DeleteChar(ConteinerClass container) {

		StringBuilder strMew = new StringBuilder();
		String charS = "";
		char c1 = '!';
		char c2 = '?';
		char c3 = '@';
		String s = null;
		s = container.mas[0];
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != c1 && s.charAt(i) != c2 && s.charAt(i) != c3) {
				charS += s.charAt(i);
			}
		}

		System.out.println(charS);
		StringBuffer str = new StringBuffer(charS);
		removeWhiteSpaces(str);
		return strMew.toString();
	}

	// ��������� �����
	static void removeWhiteSpaces(StringBuffer sb) {
		String c3 = ",";
		StringBuilder result = new StringBuilder();
		boolean prevCharIsDelimiter = false;

		int timesSpace = 0;
		String res = "";

		for (int i = 0; i < sb.length(); i++) {
			char c = sb.charAt(i);

			if (c == ' ') {
				timesSpace++;
				if (timesSpace < 2)
					res += c;
			} else {
				res += c;
				timesSpace = 0;
			}
		}

		for (int i = 0; i < res.length(); i++) {
			char ch = res.charAt(i);

			if (c3.indexOf(ch) != -1)
				prevCharIsDelimiter = true;
			else {
				if (ch != ' ' && prevCharIsDelimiter) {
					result.append(' ');
				}

				prevCharIsDelimiter = false;
			}
			result.append(ch);
		}
		System.out.println(result);
	}

}
