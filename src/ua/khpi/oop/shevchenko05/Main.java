package ua.khpi.oop.shevchenko05;



/**
 * @mainpage
 * ����������� ������ �5
 * �������� ������� ����������. ��������� 
 * ����: ������� ������� �������� ������� ���������� �� ������������ ���������. 
 * 1 ������
 * 1.	��������� ����-���������, �� ��������� ��� ���������� ���������� ����� �������� �.�. �3 � ������ ������ ����� � ��������� ���������, ��������� � ���� ��������. 
 * 2.	� ��������� ���������� �� ���������������� �������� ������: 
 * �	String toString() ������� ���� ���������� � ������ �����; 
 * �	void add(String string) ���� �������� ������� �� ���� ����������; 
 * �	void clear() ������� �� �������� � ����������; 
 * �	boolean remove(String string) ������� ������ ������� ��������� �������� � ����������; 
 * �	Object[] toArray() ������� �����, �� ������ �� �������� � ���������; 
 * �	int size() ������� ������� �������� � ���������; 
 * �	boolean contains(String string) ������� true, ���� ��������� ������ �������� �������; 
 * �	boolean containsAll(Container container) ������� true, ���� ��������� ������ �� �������� � ����������� � ����������; 
 * �	public Iterator<String> iterator() ������� �������� �������� �� Interface Iterable. 
 * 3.	� ���� ��������� �������� �� Interface Iterator ���������� ������: 
 * �	public boolean hasNext(); 
 * �	public String next(); 
 * �	public void remove(). 
 * 4.	���������������� ������ ��������� �� ��������� ����� while � for each. 
 * 5.	������������� ������������ ���������� (��������) � ��������� � Java Collections Framework.
 *
 * @author Anastasia Shevchenko
 * @version 1.0 16/10/19
 * @data 16/10/19
 * 
 */

/**
 * ����� Main �������� ����� ���������, ������ ������ ����� Main
 * 
 * @author Shevchenko Anastasia
 * @version 1.0 16/10/19
 */

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConteinerClass container = new ConteinerClass();
		ConteinerClass container1 = new ConteinerClass();
		container.add("My            name,is Anas@@@@@@@@tasia");
		container.add("Hello, world");

		
		// container.clear();//������� �� �������� � ����������;

		// container.toArray();//������� �����, �� ������ �� �������� � ���������;
	
		//container.size();//������� ����� ����������
		
		container.remove("Hello, world");//������� ������ ������� ��������� �������� � ����������;
//		container.containsAll(container, container1);//������� true, ���� ��������� ������ �� �������� � ����������� � ����������;
	
		for (String string : container)
			System.out.println(string);
		
		IteratorClass it = container.iterator();
		while(it.hasNext())
		{
			//it.next();
			System.out.println(it.next());
		}
		ConteinerClass.DeleteChar(container);

	}

}
