package ua.khpi.oop.shevchenko05;


import java.util.Iterator;

public class IteratorClass implements Iterator<String> {
	ConteinerClass container;
	int currentNumber = 0;

	public IteratorClass(ConteinerClass container) {
		// TODO Auto-generated constructor stub
		this.container = container;
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return currentNumber < this.container.mas.length;
	}

	@Override
	public String next() {
		// TODO Auto-generated method stub
	
		return this.container.mas[currentNumber++];
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}
}