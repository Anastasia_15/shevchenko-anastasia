package ua.khpi.oop.shevchenko02;

import java.util.Random;

/**
 * ���� Main �������� ����� ���������, ������: ����� main
 * 
 * @author Shevchenco Anastasia
 * @version 1.0 05/10/19
 */
public class Main {

	/**
	 * M���� main ������� �������
	 * <p>
	 * ������������ ��������� �� 1. ���������� ��������� ������ 2. ��������� 2-�
	 * �������� �����(10 ���) 3. ����������� ��� ��� 2-� �����(10 ���) 4. ���������
	 * ���������� � ������ �������
	 * 
	 * @return ��� void ������ �� �������. ������ ���������� ��������.
	 */
	public static void main(String[] args) {
		int numb_1 = 0;// �� ����� ��� ���� ����� ������
		int numb_2 = 0;
		int min = 10;// ̳�������� �������� ��� ���� ����������� �������
		int max = 99;// ����������� �������� ��� ���� ����������� �������
		int diff = max - min;
		Random rand = new Random();// ������
		System.out.println("����� ��� �����" + "         ���");
		/*
		 * � ���� ����������� ��������� ��������� ���� ������ ������ ����� �
		 * �������� �� 10 �� 99 ���� � ���������� ���� ����������� ��� ���� �����
		 * ��������� ���������� �� �����
		 */
		for (int i = 0; i < 10; i++) {
			numb_1 = rand.nextInt(diff + 1);// ��������� ������� �����
			numb_1 += min;
			numb_2 = rand.nextInt(diff + 1);// ��������� ������� �����
			numb_2 += min;
			System.out.print(numb_1 + "  " + numb_2 + "             ->    ");

			// ����������� ���
			while (numb_1 != 0 && numb_2 != 0) {
				if (numb_1 >= numb_2) {
					numb_1 = numb_1 % numb_2;
				} else {
					numb_2 = numb_2 % numb_1;
				}
			}

			if (numb_1 == 0) {
				System.out.println(numb_2);
			} else {
				System.out.println(numb_1);
			}
		}

	}

}
