package ua.khpi.oop.shevchenko13;

import java.beans.XMLEncoder;
import java.beans.XMLDecoder;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.lang.Runnable;

import ua.khpi.oop.shevchenko13.Dovnik;
import ua.khpi.oop.shevchenko13.MyThread;


public class Main {
	static MyThread mSecondThread;
	static MyThread mThirdThread;
	static MyThread mFourthThread;

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		mSecondThread = new MyThread();	//�������� ������
		mSecondThread.start();					//������ ������
		mThirdThread = new MyThread();	//�������� ������
		mThirdThread.start();					//������ ������
		mFourthThread = new MyThread();	//�������� ������
		mFourthThread.start();					//������ ������
		
		System.out.println("������� ����� ��������");
		
		Scanner in = new Scanner(System.in);
		LinkedList<Dovnik> agency = new LinkedList<Dovnik>();
		String str=null;
		int s =0;

		boolean loop = true;

		String name= null;
		String address= null;
		int working_conditions= 0;
		String phone= null;
		String days= null;
		String specialty= null;
		

		Helper helper = new Helper();
		boolean result = false;
		String regex1 = "[�-ߥ����]{1}[�-������]+[ ]?[�-ߥ�����-������]*";
		String regex2 = "[1-5]{1}[0-9]+";
		String regex3 = "[�-ߥ����]{1}[�-ߥ�����-������]+[ ]?[0-9]*";
		String regex5 = "[+380]?[0-9]+";
		String regex6 = "(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?";

		boolean contains1 = Arrays.asList(args).contains("auto");

		if (contains1 == true) {
			helper.readFromFile(agency);
			helper.sortByName(agency);
			helper.sortBySpecialty(agency);
			helper.sortByAddress(agency);
			mSecondThread.search(agency);
			System.out.println("������� ����������� ����� ���������� (���):");
			int m = in.nextInt();
			mThirdThread.min(agency);
			try{
				Thread.sleep(m*1000); //�������� � ������� m ���.
			}catch(InterruptedException e){}
			
			mThirdThread.interrupt();
			mFourthThread.count(agency);
		}

		else {
			while (loop) {
				System.out.println("������ �������� �������� �����? ");
				str = in.nextLine();
				if (str.equals("��") || str.equals("��")) {
					System.out.println("������� ��������: ");
					str = in.nextLine();
					result = str.matches(regex1);
					while(result == false) {
						System.out.println("�� ����� ������������ �������� �����! ���������� ��� ���.");
						str = in.nextLine();
						result = str.matches(regex1);
					}
					name = str;
					
					System.out.println("������� �����: ");
					str = in.nextLine();
					result = str.matches(regex3);
					while(result == false) {
						System.out.println("�� ����� ������������ �����! ���������� ��� ���.");
						str = in.nextLine();
						result = str.matches(regex3);
					}
					address = str;
					
					System.out.println("������� ������� ����� (���������� ������� �����): ");
					str = in.nextLine();
					result = str.matches(regex2);
					while(result == false) {
						System.out.println("�� ����� ������������ ���-��� ������� �����! ���������� ��� ���.");
						s = in.nextInt();
						result = str.matches(regex2);
					}
					working_conditions = s;
					
					System.out.println("������� ����� ��������: ");
					str = in.nextLine();
					result = str.matches(regex5);
					while(result == false) {
						System.out.println("�� ����� ����������� �����! ���������� ��� ���.");
						str = in.nextLine();
						result = str.matches(regex5);
					}
					phone = str;				
				
					System.out.println("������� ������� ��� ������: ");
					str = in.nextLine();
					result = str.matches(regex6);
					while(result == false) {
						System.out.println("�� ����� ������������ ���! ���������� ��� ���.");
						str = in.nextLine();
						result = str.matches(regex6);
					}
					days = str;
					
					System.out.println("������� ����� �������������: ");				
					str = in.nextLine();
					result = str.matches(regex1);
					while(result == false) {
						System.out.println("�� ����� ������������ �������������! ���������� ��� ���.");
						str = in.nextLine();
						result = str.matches(regex1);
					}
					specialty = str;
					
					Dovnik vacancy = new Dovnik(name, address, working_conditions, phone, days, specialty);
					agency.add(vacancy);
				} else
					loop = false;
			}
			helper.sortByName(agency);
			helper.sortBySpecialty(agency);
			helper.sortByAddress(agency);
			helper.search(agency);
		}


		// agency.remove(0);
		// agency.removeAll(agency);
		// System.out.println("Linked list : " + agency);
		// agency.contains(w);

		for (int i = 0; i < agency.size(); i++) {
			System.out.println("�" + (i + 1));
			System.out.println(agency.get(i));
			System.out.println();
		}

		/*
		 * for (Vacancy vacancy : agency) System.out.println(vacancy);
		 */

		String path = "Beanarchive.xml";
		XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(path)));
		for (int i = 0; i < agency.size(); i++) {
			encoder.writeObject(agency.get(i));
		}
		encoder.close();

		XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(path)));
		LinkedList<Dovnik> agencyRestored = new LinkedList<Dovnik>();
		Dovnik a = new Dovnik();
		for (int i = 0; i < agency.size(); i++) {
			a = (Dovnik) decoder.readObject();
			agencyRestored.add(a);
		}
	
		decoder.close();
		System.out.println("After Restored: ");
		for (int i = 0; i < agencyRestored.size(); i++) {
			System.out.println(agencyRestored.get(i));
		}


	}

}
