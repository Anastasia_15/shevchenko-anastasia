package ua.khpi.oop.shevchenko10;

import java.beans.XMLEncoder;
import java.beans.XMLDecoder;
import java.io.*;
import java.util.*;

import ua.khpi.oop.shevchenko09.Dovnik;



public class Main {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		LinkedList<Dovnik> agency = new LinkedList<Dovnik>();
		String str;

		boolean loop = true;

		String name= null;
		String address= null;
		String working_conditions= null;
		String phone= null;
		String days= null;
		String specialty= null;
		

		Helper helper = new Helper();

		boolean contains1 = Arrays.asList(args).contains("auto");

		if (contains1 == true) {
			helper.readFromFile(agency);
			helper.sortByName(agency);
			helper.sortBySpecialty(agency);
			helper.sortByAddress(agency);
		}

		else {
			while (loop) {
				System.out.println("������ �������� �������� �����? ");
				str = in.nextLine();
				if (str.equals("��") || str.equals("��")) {
					System.out.println("������� ��������: ");
					str = in.nextLine();
					name = str;
					System.out.println("������� �����: ");
					str = in.nextLine();
					address = str;
					System.out.println("������� ������� ����� (���������� ������� �����): ");
					str = in.nextLine();
					working_conditions = str;
					System.out.println("������� ����� ��������: ");
					str = in.nextLine();
					phone = str;				
				
					System.out.println("������� ������� ��� ������: ");
					str = in.nextLine();
					days = str;
					System.out.println("������� �������������: ");				
					str = in.nextLine();
					
					specialty = str;
					Dovnik vacancy = new Dovnik(name, address, working_conditions, phone, days, specialty);
					agency.add(vacancy);
				} else
					loop = false;
			}
			helper.sortByName(agency);
			helper.sortBySpecialty(agency);
			helper.sortByAddress(agency);
		}

		/*
		 * FileWriter writer = new FileWriter("file.txt"); for(int i = 0; i < size; i++)
		 * { writer.write(agency.get(i).getFirm().toString() + ", " +
		 * agency.get(i).getSpecialty().toString() + ", " +
		 * agency.get(i).getWorking_conditions() + ", " + agency.get(i).getWage() + ", "
		 * + agency.get(i).getExperience() + ", " +
		 * agency.get(i).getEducation().toString()); writer.append('\n'); }
		 * writer.close();
		 */

		/*
		 * FileReader reader = new FileReader("file.txt"); Scanner scanner = new
		 * Scanner(reader); int j = 1; while(scanner.hasNextLine()) { //Vacancy v = new
		 * Vacancy(scanner.nextLine()); //agency.set(j, v); firm = scanner.nextLine();
		 * j++; }
		 */

		// agency.remove(0);
		// agency.removeAll(agency);
		// System.out.println("Linked list : " + agency);
		// agency.contains(w);

		for (int i = 0; i < agency.size(); i++) {
			System.out.println("�" + (i + 1));
			System.out.println(agency.get(i));
			System.out.println();
		}

		/*
		 * for (Vacancy vacancy : agency) System.out.println(vacancy);
		 */

		String path = "Beanarchive.xml";
		XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(path)));
		for (int i = 0; i < agency.size(); i++) {
			encoder.writeObject(agency.get(i));
		}
		encoder.close();

		XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(path)));
		LinkedList<Dovnik> agencyRestored = new LinkedList<Dovnik>();
		Dovnik a = new Dovnik();
		for (int i = 0; i < agency.size(); i++) {
			a = (Dovnik) decoder.readObject();
			agencyRestored.add(a);
		}
		/*
		 * //LinkedList<Vacancy> agencyRestored = (LinkedList<Vacancy>)
		 * decoder.readObject(); Vacancy m = (Vacancy) decoder.readObject(); Vacancy n =
		 * (Vacancy) decoder.readObject();
		 */
		decoder.close();
		System.out.println("After Restored: ");
		for (int i = 0; i < agencyRestored.size(); i++) {
			System.out.println(agencyRestored.get(i));
		}

		/*
		 * //������������ � ���� � ������� ������ ObjectOutputStream ObjectOutputStream
		 * objectOutputStream = new ObjectOutputStream(new
		 * FileOutputStream("container.out")); objectOutputStream.writeObject(agency);
		 * objectOutputStream.close();
		 * 
		 * // ������������� �� ����� � ������� ������ ObjectInputStream
		 * ObjectInputStream objectInputStream = new ObjectInputStream(new
		 * FileInputStream("container.out")); LinkedList<Vacancy> agencyRestored =
		 * (LinkedList<Vacancy>) objectInputStream.readObject();
		 * objectInputStream.close();
		 * 
		 * System.out.println("Before Serialize: " + "\n" + agency + "\n");
		 * System.out.println("After Restored: " + "\n" + agencyRestored + "\n");
		 */

	}

}
