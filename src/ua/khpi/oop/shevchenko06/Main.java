/**
 * 
 */
package ua.khpi.oop.shevchenko06;

import java.util.Scanner;

import ua.khpi.oop.shevchenko04.HelperClass;

/**
 * @mainpage
 * ����������� ������ �6
 * ����������/������������ �ᒺ���. ��������� ����� �����������
 * ����: ������� ��������� �� ���������� ����� �ᒺ���.
 * 		 ������������ � ���������� ����������/������������ �ᒺ���.
 * 		 ������������ ������� ����� �����������.
 * 1 ������: 
 *		1) ���������� � ���������������� ������� ���������/
 *		   ���������� ������ ������������ ���������� �� ��������� ����������/������������.
 *		2) ��������� �������������� (��� ����������� ����) 
 *		   ��������� ������ (Utility Class) ������ ������ �.�. 
 *         �3 � ����� ��������� (������� ��������).
 *		3) ���������������� ��������� �� �������� ������� 
 *		   �������� ������������ ���������� �� ��������� �������� � 
 *		   ���������� �� ������ ���������� �����.
 *		4) ���������� �� ���������������� ���������, ���������� �� ����� �������� � ���������.
 *		5) ��������� ��������� �������� �� ����������� 
 *		   ��������� ����� ������ � ������������ ��� ������������ �� ���������� ������.
 *
 * @author Anastasia Shevchenko
 * @version 1.0 16/10/19
 * @data 16/10/19
 * 
 */

/**
 * ����� Main �������� ����� ���������, ������ ������ ����� Main
 * 
 * @author Shevchenko Anastasia
 * @version 1.0 16/11/19
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO ������������� ��������� �������� ������
		// TODO Auto-generated method stub
		ConteinerClass container = new ConteinerClass();
		ConteinerClass container1 = new ConteinerClass();

		String str = null;
		boolean a = true;
		Scanner in = new Scanner(System.in);
		Scanner ins = new Scanner(System.in);

		while (a) {
			System.out.println();
			ConteinerClass.ChooseMenu();
			System.out.println();
			int answer = in.nextInt();
			System.out.println();

			switch (answer) {
			case 1:
				container.add("My            name,is Anas@@@@@@@@tasia");
				container.add("Hello, world");
				container.add("Hello, Paint");

			case 2:
				for (String string : container)
					System.out.println(string);

				IteratorClass it = container.iterator();
				while (it.hasNext()) {
					System.out.println(it.next());
				}

			case 3:
				ConteinerClass.DeleteChar(container);

			case 4:
				container.clear();// ������� �� �������� � ����������;

			case 5:
				container.toArray();// ������� �����, �� ������ �� �������� � ���������;

			case 6:
				container.size();// ������� ����� ����������

			case 7:
				container.remove("Hello world");// ������� ������ ������� ��������� �������� � ����������;

			case 8:
				container.containsAll(container, container1);// ������� true, ���� ��������� ������ �� �������� �
															// ����������� � ����������;

			case 9://����������
				ConteinerClass.Sort(container);
				
			case 10:// ����� �� �������
				ConteinerClass.Search(container);
				
			case 11:
				a = false;
				System.out.println("������� �� ������");

			}
		}
		in.close();
		ins.close();
	}
}
