package ua.khpi.oop.shevchenko07;

public class Requirement {
	Requirement(){}
	
	private String specialty;
	private int phone;
	private String days;
	
	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String a) {
		specialty = a;
	}
	
	public int getPhone() {
		return phone;
	}

	public void setPhone(int a) {
		phone = a;
	}
	
	public String getDays() {
		return days;
	}

	public void setDays(String a) {
		days = a;
	}
}
