package ua.khpi.oop.shevchenko04;

import java.util.Scanner;
import java.util.Arrays;

/**
 * ���� Main �������� ����� ���������, ������: ����� main
 * 
 * @author Shevchenco Anastasia
 * @date 06.11.2019
 * @version 1.0 05/10/19
 */
public class Main {

	/**
	 * M���� main ������� �������
	 * <p>
	 * ������������ ��������� �� 
	 * 1. ���������� ������ help � debug
	 * 2. ��������� ����� � �������� ���������� ���� � ����� 
	 * 	  � ��������� �� ������ ����������� ���� ������������ ����� ��:
	 * 			ĳ� 1. �������� ����� 
	 * 			ĳ� 2. �������� �������� �����
	 * 			ĳ� 3. ��������� ���������
	 * 			ĳ� 4. ���������� ������
	 * 
	 * @return ��� void ������ �� �������. ������ ���������� ��������.
	 */
	public static void main(String[] args) {
		// TODO ������������� ��������� �������� ������
		boolean contains1 = Arrays.asList(args).contains("help");
		boolean contains2 = Arrays.asList(args).contains("debug");

		if (contains1 == true) {
			System.out.println("����������� ������ �4\r\n"
					+ "  ����: ������������ ��������� �������� ��� ��������� Java SE\r\n" + "  \r\n"
					+ "  ����: ��������� ���������� ������ ������ � ������������ � ���������� ��������� ����� Java.\r\n"
					+ "  ������: \r\n"
					+ "  1) �������������� �������� ������ �������� ����������� ������ �3, �������� �� ��������� ������ ����������� ������� ������ ����������� � ������ ���������� ����:\r\n"
					+ "  �������� �����;\r\n" + "  �������� �����;\r\n" + "  ��������� ���������;\r\n"
					+ "  ����������� ����������;\r\n" + "  ���������� �������� � �.�.\r\n"
					+ "  2) ����������� ������� ��������� ���������� ����� ��� ���������� ������ ������ ��������:\r\n"
					+ "  �������� �-h� �� �-help�: ������������ ���������� ��� ������ ��������, ����������� (������������ ��������), ��������� ���� ������ ������ (������ ���� �� ��������� ���������� �����);\r\n"
					+ "  �������� �-d� �� �-debug�: � ������ ������ �������� ������������� �������� ����, �� ���������� ������������ �� �������� ������������� ��������: ����������� �����������, ������� �������� ������, �������� ���������� ������ �� ��.\n"
					+ "  �����: �������� ���������\n");
		}

		if (contains2 == true) {
			System.out.println("��������� �������� � ������ debug");
		}
		String str = null;
		boolean a = true;
		Scanner in = new Scanner(System.in);
		Scanner ins = new Scanner(System.in);
		// String str = "jhgf fghj @@2fghf@@ghj jhg,hfghh hg,hjk,d!";
		while (a) {
			System.out.println();
			HelperClass.ChooseMenu();
			System.out.println();
			int answer = in.nextInt();
			System.out.println();

			switch (answer) {

			case 1:
				System.out.println("������ �����: ");
				str = ins.nextLine();
				// System.out.println(str);
				break;
			case 2:
				System.out.println("��� �����: ");
				HelperClass.inStr(str);
				System.out.println();
				break;

			case 3:
				HelperClass.DeleteCharacter(str);
				System.out.println("���������� ��������!");
				System.out.println();
				break;

			case 4:
				a = false;
				System.out.println("������� �� ������");
			}

		}
		in.close();
	}

}
