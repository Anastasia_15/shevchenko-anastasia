package ua.khpi.oop.shevchenko04;

public class HelperClass {

	/**
	 * M���� ChooseMenu
	 * <p>
	 * ĳ������� ���� � ������:
	 * 		1. �������� �����
	 * 		2. �������� �����
	 * 		3. ��������� ���������
	 * 		4. �����
	 * @return ��� void ������ �� �������.
	 */
	static void ChooseMenu() {
		System.out.println("1. �������� �����");
		System.out.println("2. �������� �����");
		System.out.println("3. ��������� ���������");
		System.out.println("4. �����");
	}

	/**
	 * M���� inStr
	 * <p>
	 *  ������������ ��������� ��:
	 *  1. ����� ������� ���� �� ����� �������� �����
	 * @return ��� void ������ �� �������.
	 */
	static void inStr(String str) {
		System.out.println(str);
	}

	/**
	 * M���� DeleteCharacter
	 * <p>
	 * ������������ ��������� �� 1. ���������� ��������� ������(������� ��
	 * ����� ��������) 2. � ���� ���������� ��������� �������� ������� 3.
	 * ��������� ����� ��� ������ ������� 4. ������ ������ removeWhiteSpaces
	 * 
	 * @return ������� ���� ���������� � ������ �����;
	 */
	public static String DeleteCharacter(String s) {
		StringBuilder strMew = new StringBuilder();
		String charS = "";
		char c1 = '!';
		char c2 = '?';
		char c3 = '@';

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != c1 && s.charAt(i) != c2 && s.charAt(i) != c3) {
				charS += s.charAt(i);
			}
		}

		System.out.println(charS);
		StringBuffer str = new StringBuffer(charS);
		removeWhiteSpaces(str);
		return strMew.toString();
	}

	/**
	 * M���� removeWhiteSpaces
	 * <p>
	 * ������������ ��������� �� 
	 * 1. ���������� ��������� ������ (end,spaceIndex)
	 * 2. ��������� ������ ��������. ���� ������� �� ������� �� ���� ��������
	 * ��������� ���� ����. 
	 * 3. ��������� ����� ��� ������ ��������
	 * 
	 * @return ��� void ������ �� �������. ������ ���������� ������.
	 */
	static void removeWhiteSpaces(StringBuffer sb) {
		int end = 0;
		int spaceIndex = -1;
		char c1 = ',';
		char c2 = ' ';
		String c3 = ",";
		StringBuilder result = new StringBuilder();
		// StringBuilder result1 = new StringBuilder();
		boolean prevCharIsDelimiter = false;

		int timesSpace = 0;
		String res = "";

		for (int i = 0; i < sb.length(); i++) {
			char c = sb.charAt(i);

			if (c == ' ') {
				timesSpace++;
				if (timesSpace < 2)
					res += c;
			} else {
				res += c;
				timesSpace = 0;
			}
		}

		for (int i = 0; i < res.length(); i++) {
			char ch = res.charAt(i);

			if (c3.indexOf(ch) != -1)
				prevCharIsDelimiter = true;
			else {
				if (ch != ' ' && prevCharIsDelimiter) {
					result.append(' ');
				}

				prevCharIsDelimiter = false;
			}
			result.append(ch);
		}
		System.out.println(result);
	}

}
