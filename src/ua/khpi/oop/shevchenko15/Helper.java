package ua.khpi.oop.shevchenko15;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

import ua.khpi.oop.shevchenko15.Dovnik;

public class Helper {
	void sortByName (LinkedList<Dovnik>agency) {
		int size = agency.size();
		String arr[] = new String[size];
		for(int i = 0; i < size; i++) {
			arr[i] = agency.get(i).getName();
		}
		Arrays.sort(arr);
		System.out.printf("sorted by specialty : \n%s\n\n", 
                Arrays.toString(arr)); 
	}
	
	void sortBySpecialty (LinkedList<Dovnik>agency) {
		int size = agency.size();
		String arr[] = new String[size];
		for(int i = 0; i < size; i++) {
			arr[i] = agency.get(i).getEducation();
		}
		Arrays.sort(arr);
		System.out.printf("sorted by specialty : \n%s\n\n", 
                Arrays.toString(arr)); 
	}
	
	void sortByAddress (LinkedList<Dovnik>agency) {
		int size = agency.size();
		String arr[] = new String[size];
		for(int i = 0; i < size; i++) {
			arr[i] = agency.get(i).getAddress();
		}
		Arrays.sort(arr);
		System.out.printf("sorted by Address : \n%s\n\n", 
                Arrays.toString(arr)); 
	}
	
	void search(LinkedList<Dovnik> agency) {
		int size = agency.size();
		ArrayList<Integer> arr1 = new ArrayList<Integer>();
		String regex = "[0-9]{5}";
		boolean result = false;
		String str=null;

			for (int i = 0; i < size; i++) {
				str = agency.get(i).getPhone();
				result = str.matches(regex);
				if (agency.get(i).getDays().contains("Mon Tues Wed Thurs Fri Sat Sun")&& result == true) {
					arr1.add(i+1);
				}
				if (agency.get(i).getDays().contains("Mon Tues Wed Thurs Fri Sat Sun") && agency.get(i).getPhone().contains("+3")) {
					arr1.add(i+1);
				}
				
			}

		if(arr1.size() == 0) System.out.printf("Firm not found\n");
		if(arr1.size() == 1)System.out.printf("Firm found with number: \n%s\n\n", arr1);
		if(arr1.size() > 1)System.out.printf("Firm found with number: \n%s\n\n", arr1);
	}
	
	void readFromFile(LinkedList<Dovnik>agency) throws IOException, ClassNotFoundException{
		String string = null;
		String str = null;
		String str1 = null;
		String name= null;
		String address= null;
		int working_conditions= 0;
		String phone= null;
		String days= null;
		String specialty= null;
		boolean result = false;
		String regex1 = "[A-Z]{1}[a-z]+[ ]?[A-Za-z]*";
		String regex2 = "[1-5]{1}[0-9]+";
		String regex3 = "[A-Z]{1}[A-Za-z]+[ ]?[0-9]*";
		String regex5 = "(+380)?[0-9]+";
		String regex6 = "(Mon)?[ ]?(Tues)?[ ]?(Wed)?[ ]?(Thurs)?[ ]?(Fri)?[ ]?(Sat)?[ ]?(Sun)?[ ]?";
		
		BufferedReader br = new BufferedReader(new FileReader("file.txt"));
		BufferedReader br1 = new BufferedReader(new FileReader("file.txt"));
		int character;
		StringBuilder sb = new StringBuilder();
			while ((str = br.readLine()) != null) {
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				string = sb.toString();
				
				name = sb.toString();
				sb = new StringBuilder();
				
				
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				string = sb.toString();
				specialty = sb.toString();
				sb = new StringBuilder();
				
				
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				int n = Integer.parseInt(sb.toString());
				str1 = Integer.toString(n);
				working_conditions = n;
				sb = new StringBuilder();
				
				
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				string = sb.toString();
	
				address = sb.toString();
				sb = new StringBuilder();
				
				
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				string = sb.toString();
	
				phone = sb.toString();
				sb = new StringBuilder();
				
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',' || character == '\n')
						break;
					sb.append(ch);
				}
				string = sb.toString();
		
				days = sb.toString();
				sb = new StringBuilder();
				
				Dovnik v = new Dovnik(name, address, working_conditions, phone, days, specialty);
				agency.add(v);
			}
	}
}