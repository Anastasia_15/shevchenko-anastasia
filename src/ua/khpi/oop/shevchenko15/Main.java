package ua.khpi.oop.shevchenko15;


import java.beans.XMLEncoder;
import java.beans.XMLDecoder;
import java.io.*;
import java.util.*;

import ua.khpi.oop.shevchenko15.Dovnik;



public class Main {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		LinkedList<Dovnik> agency = new LinkedList<Dovnik>();
		String str = null;
		int s = 0;

		boolean loop = true;

		String name= null;
		String address= null;
		int working_conditions= 0;
		String phone= null;
		String days= null;
		String specialty= null;
		

		Helper helper = new Helper();
		boolean result = false;
		String regex1 = "[�-ߥ����]{1}[�-������]+[ ]?[�-ߥ�����-������]*";
		String regex2 = "[1-5]{1}[0-9]+";
		String regex3 = "[�-ߥ����]{1}[�-ߥ�����-������]+[ ]?[0-9]*";
		String regex5 = "[+380]?[0-9]+";
		String regex6 = "(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?";

		boolean contains1 = Arrays.asList(args).contains("auto");

		if (contains1 == true) {
			helper.readFromFile(agency);
			helper.sortByName(agency);
			helper.sortBySpecialty(agency);
			helper.sortByAddress(agency);
			helper.search(agency);
		}

		else {
			while (loop) {
				System.out.println("1. �������� ��������");
				System.out.println("2. ������� �������� �� �������");
				System.out.println("3. �������� ������");
				System.out.println("4. �������������");
				System.out.println("5. ����� ���. ���������");
				System.out.println("6. �����");
				str = in.nextLine();
				switch(str) {
				case "1" :
					System.out.println("");
					System.out.println("");
					System.out.println("������� ��������: ");
					str = in.nextLine();
					result = str.matches(regex1);
					while(result == false) {
						System.out.println("�� ����� ������������ �������� �����! ���������� ��� ���.");
						str = in.nextLine();
						result = str.matches(regex1);
					}
					name = str;
					
					System.out.println("������� �����: ");
					str = in.nextLine();
					result = str.matches(regex3);
					while(result == false) {
						System.out.println("�� ����� ������������ �����! ���������� ��� ���.");
						str = in.nextLine();
						result = str.matches(regex3);
					}
					address = str;
					
					System.out.println("������� ������� ����� (���������� ������� �����): ");
					str = in.nextLine();
					result = str.matches(regex2);
					while(result == false) {
						System.out.println("�� ����� ������������ ���-��� ������� �����! ���������� ��� ���.");
						str = in.nextLine();
						result = str.matches(regex2);
					}
					working_conditions = s;
					
					System.out.println("������� ����� ��������: ");
					str = in.nextLine();
					result = str.matches(regex5);
					while(result == false) {
						System.out.println("�� ����� ����������� �����! ���������� ��� ���.");
						str = in.nextLine();
						result = str.matches(regex5);
					}
					phone = str;				
				
					System.out.println("������� ������� ��� ������: ");
					str = in.nextLine();
					result = str.matches(regex6);
					while(result == false) {
						System.out.println("�� ����� ������������ ���! ���������� ��� ���.");
						str = in.nextLine();
						result = str.matches(regex6);
					}
					days = str;
					
					System.out.println("������� �������������: ");				
					str = in.nextLine();
					result = str.matches(regex1);
					while(result == false) {
						System.out.println("�� ����� ������������ �������������! ���������� ��� ���.");
						str = in.nextLine();
						result = str.matches(regex1);
					}
					specialty = str;
					
					Dovnik vacancy = new Dovnik(name, address, working_conditions, phone, days, specialty);
					agency.add(vacancy);
					break;
				
				case "2" :
					System.out.println("");
					System.out.println("");
					System.out.println("������� ����� �������� ��� ��������: ");	
					s = in.nextInt();
					agency.remove(s);
					System.out.println("������� �����!");
					break;
					
				case "3":
					System.out.println("");
					agency.removeAll(agency);
					System.out.println("������ ������!");
					break;
					
				case "4":
					System.out.println("");
					helper.sortByName(agency);
					helper.sortBySpecialty(agency);
					helper.sortByAddress(agency);
					break;
					
				case "5":
					System.out.println("");
					helper.search(agency);
					break;
					
				case "6":
					System.out.println("");
					System.out.println("������� �� ������!");
					loop = false;
				}					
			}	
		}


		for (int i = 0; i < agency.size(); i++) {
			System.out.println("�" + (i + 1));
			System.out.println(agency.get(i));
			System.out.println();
		}



		String path = "Beanarchive.xml";
		XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(path)));
		for (int i = 0; i < agency.size(); i++) {
			encoder.writeObject(agency.get(i));
		}
		encoder.close();

		XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(path)));
		LinkedList<Dovnik> agencyRestored = new LinkedList<Dovnik>();
		Dovnik a = new Dovnik();
		for (int i = 0; i < agency.size(); i++) {
			a = (Dovnik) decoder.readObject();
			agencyRestored.add(a);
		}

		decoder.close();
		System.out.println("After Restored: ");
		for (int i = 0; i < agencyRestored.size(); i++) {
			System.out.println(agencyRestored.get(i));
		}


	}

}
