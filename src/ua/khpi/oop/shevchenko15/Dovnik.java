package ua.khpi.oop.shevchenko15;


import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;

public class Dovnik implements Externalizable{ 
	public Dovnik() {}
	private String name;//
	private String address;//
	private int working_conditions;//
	private String phone;//
	private String days;///yes
	private String specialty;//
	
	public Dovnik(String name, String address, int working_conditions2, String phone2, String days, String specialty) {
		this.name = name;
		this.address = address;
		this.working_conditions = working_conditions2;
		this.phone = phone2;
		this.days = days;
		this.specialty = specialty;
	}
	@Override
	public String toString() {
		return "�������� �����: " + name + "\n ������: " + address + "\n ����� ������: " + working_conditions
				+ "\n �������: " + phone + "\n ������� ���: " + days + "\n �������: " + specialty + "\n";
	}

	public String getName() {
		return name;
	}
	public void setnName(String name) {
		this.name = name;
	}
	public int getWorking_conditions() {
		return working_conditions;
	}
	public void setWorking_conditions(int working_conditions) {
		this.working_conditions = working_conditions;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getEducation() {
		return specialty;
	}
	public void setSpecialty(String education) {
		this.specialty = education;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(name);
		out.writeObject(address);
		out.writeObject(working_conditions);
		out.writeObject(phone);
		out.writeObject(days);
		out.writeObject(specialty);
	}
	
	@Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		name = (String) in.readObject();
		address = (String) in.readObject();
        working_conditions = (int) in.readObject();
        phone = (String) in.readObject();
        days = (String) in.readObject();
        specialty = (String) in.readObject();
        
    }

}