package ua.khpi.oop.shevchenko11;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

import ua.khpi.oop.shevchenko09.Dovnik;

public class Helper {
	void sortByName (LinkedList<Dovnik>agency) {
		int size = agency.size();
		String arr[] = new String[size];
		for(int i = 0; i < size; i++) {
			arr[i] = agency.get(i).getName();
		}
		Arrays.sort(arr);
		System.out.printf("Vacancies sorted by firm : \n%s\n\n", 
                Arrays.toString(arr)); 
	}
	
	void sortBySpecialty (LinkedList<Dovnik>agency) {
		int size = agency.size();
		String arr[] = new String[size];
		for(int i = 0; i < size; i++) {
			arr[i] = agency.get(i).getEducation();
		}
		Arrays.sort(arr);
		System.out.printf("Vacancies sorted by specialty : \n%s\n\n", 
                Arrays.toString(arr)); 
	}
	
	void sortByAddress (LinkedList<Dovnik>agency) {
		int size = agency.size();
		String arr[] = new String[size];
		for(int i = 0; i < size; i++) {
			arr[i] = agency.get(i).getAddress();
		}
		Arrays.sort(arr);
		System.out.printf("Vacancies sorted by Address : \n%s\n\n", 
                Arrays.toString(arr)); 
	}
	
	void readFromFile(LinkedList<Dovnik>agency) throws IOException, ClassNotFoundException{
		String string = null;
		String str = null;
		String name= null;
		String address= null;
		String working_conditions= null;
		String phone= null;
		String days= null;
		String specialty= null;
		boolean result = false;
		String regex1 = "[�-ߥ����]{1}[�-������]+[ ]?[�-ߥ�����-������]*";
		String regex2 = "[1-5]{1}[0-9]+";
		String regex3 = "[�-ߥ����]{1}[�-ߥ�����-������]+[ ]?[0-9]*";
		String regex5 = "[0][0-9]{9}";
		String regex6 = "(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?(��)?[ ]?";
		
		BufferedReader br = new BufferedReader(new FileReader("file.txt"));
		BufferedReader br1 = new BufferedReader(new FileReader("file.txt"));
		int character;
		StringBuilder sb = new StringBuilder();
			while ((str = br.readLine()) != null) {
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				string = sb.toString();
				result = string.matches(regex1);
				if (result  == false) {
					System.out.println("�� ����� ������������ �������� �����! ���������� ��� ���.");
					break;
				}
				name = sb.toString();
				sb = new StringBuilder();
				
				
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				string = sb.toString();
				result = string.matches(regex1);
				if (result  == false) {
					System.out.println("�� ����� ������������ �������������! ���������� ��� ���.");
					break;
				}
				specialty = sb.toString();
				sb = new StringBuilder();
				
				
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				string = sb.toString();
				result = string.matches(regex2);
				if (result  == false) {
					System.out.println("�� ����� ������������ ���-��� ������� �����! ���������� ��� ���.");
					break;
				}
				working_conditions = sb.toString();
				sb = new StringBuilder();
				
				
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				string = sb.toString();
				result = string.matches(regex3);
				if (result  == false) {
					System.out.println("�� ����� ������������ �����! ���������� ��� ���.");
					break;
				}
				address = sb.toString();
				sb = new StringBuilder();
				
				
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',')
						break;
					sb.append(ch);
				}
				string = sb.toString();
				result = string.matches(regex5);
				if (result  == false) {
					System.out.println("�� ����� ����������� �����! ���������� ��� ���.");
					break;
				}
				phone = sb.toString();
				sb = new StringBuilder();
				
				while ((character = br1.read()) != -1) {
					char ch = (char) character;
					if (character == ',' || character == '\n')
						break;
					sb.append(ch);
				}
				string = sb.toString();
				result = string.matches(regex6);
				if (result  == false) {
					System.out.println("�� ����� ������������ ���! ���������� ��� ���.");
					break;
				}
				days = sb.toString();
				sb = new StringBuilder();
				
				Dovnik v = new Dovnik(name, address, working_conditions, phone, days, specialty);
				agency.add(v);
			}
	}
}