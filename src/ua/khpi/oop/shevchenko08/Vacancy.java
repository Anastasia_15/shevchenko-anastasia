package ua.khpi.oop.shevchenko08;

import java.io.*;

public class Vacancy implements Externalizable {
	Vacancy(int size) {
		
	}

	public Vacancy() {
	}

	private String denomination;
	private String address;
	private int working_conditions;
	private int headset;
	Requirement r = new Requirement();

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String a) {
		denomination = a;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String a) {
		address = a;
	}

	public int getWorking_conditions() {
		return working_conditions;
	}

	public void setWorking_conditions(int a) {
		working_conditions = a;
	}

	public int getHeadset() {
		return headset;
	}

	public void setHeadset(int a) {
		headset = a;
	}

	public void print(Vacancy a) {
		System.out.println("\n�����: " + a.getDenomination() + "\n"+"������: " + a.getAddress()+"\n"
				+ "ʳ������ ����� � �����: " + a.getWorking_conditions() + "\n"+"�������� �������: " + a.getHeadset() +"\n"
				+ "������������: " + a.r.getSpecialty() +"\n"+ "���������� �������: " + a.r.getPhone() +"\n"
				+ "������ ���: " + a.r.getDays()+"\n");
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(denomination);
		out.writeObject(address);
		out.writeObject(working_conditions);
		out.writeObject(headset);
	}
	
	@Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		denomination = (String) in.readObject();
		address = (String) in.readObject();
        working_conditions = (int) in.readObject();
        headset = (int) in.readObject();
    }

}
